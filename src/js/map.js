export const initMap = () => {
    let map;
    let markers = [];
    
    $(".coordinates li").each(function() {
        markers.push(
            new ymaps.Placemark(
                [$(this).data("lat"), $(this).data("lng")],
                {},
                {preset: 'islands#redIcon'}
            )
        );
    });

    ymaps.ready(['Map', 'GeoObjectCollection', 'Placemark', 'util.bounds']).then(function() {
        var collection = new ymaps.GeoObjectCollection({ children: markers}, { preset: 'islands#blueStretchyIcon' });
        
        const mapElement = document.getElementById('map'),
            points = collection.toArray().map(function(x) { return x.geometry.getCoordinates(); }),
            bounds = ymaps.util.bounds.fromPoints(points),
            mapRect = mapElement.getBoundingClientRect(),
            pos = ymaps.util.bounds.getCenterAndZoom(bounds, [mapRect.width, mapRect.height]);

        map = new ymaps.Map(mapElement, {
            center: pos.center,
            zoom: pos.zoom,
            controls: ['fullscreenControl']
        }, {});
        map.geoObjects.add(collection);
    });
}