// Styles
import "../css/fontello.css";
import "../css/range.css";
import "selectize/dist/css/selectize.default.css";
import "swiper/css/swiper.min.css";
import "../scss/style.scss";
// JS
import "selectize/dist/js/selectize";
import Swiper from "swiper";
import "./bootstrap/bootstrap";
import Instafeed from "instafeed.js"

import { initMap } from "./map";

$(document).ready(() => {
    // Selectize Init
    const customSelect = $(".custom-select");

    if ( customSelect.length > 0 ) {
        customSelect.selectize();
        $(".selectize-control").find("input").attr("disabled", true);
    }

    // Toggle mobile menu
    const header = $("header");
    const menuToggler = $(".menu-toggler");

    menuToggler.click(() => {
        $("body").toggleClass("no-scroll-y");
        header.toggleClass("active");
    });

    // Toggle search
    const searchToggler = $(".search-toggler");
    const searchBlock = $(".search-block");
    const closeSearchBlock = $(".close-search-block");
    const searchResults = $(".search-results");

    searchToggler.click(() => {
        searchBlock.fadeIn(150);
    });

    closeSearchBlock.click(() => {
        searchBlock.fadeOut(150);
        searchResults.removeClass("active");
    });
    
    // Intro slider
    const introSlider = new Swiper(".intro-slider .swiper-container", {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        autoplay: {
            delay: 8000
        },
        navigation: {
            prevEl: '.swiper-button-prev',
            nextEl: '.swiper-button-next'
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        }
    });

    // Services sliders
    const x4SliderOptions = {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        autoplay: {
            delay: 6000
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        spaceBetween: 24,
        breakpoints: {
            1200: {
                slidesPerView: 4
            },
            768: {
                slidesPerView: 3
            },
            575: {
                slidesPerView: 2
            },
            300: {
                slidesPerView: 1.2,
                spaceBetween: 20,
                centeredSlides: true
            }
        }
    }

    const popularExperiences = new Swiper(".popular-experiences .swiper-container", x4SliderOptions);
    $(".popular-experiences .prev-slide").click(() => popularExperiences.slidePrev());
    $(".popular-experiences .next-slide").click(() => popularExperiences.slideNext());

    const newExperiences = new Swiper(".new-experiences-slider .swiper-container", x4SliderOptions);
    $(".new-experiences-slider .prev-slide").click(() => newExperiences.slidePrev());
    $(".new-experiences-slider .next-slide").click(() => newExperiences.slideNext());

    // Instagram
    const instagram = $('.instagram');
    
    if (instagram.length > 0) {
        const userId = instagram.data('user');
        const accessToken = instagram.data('token');
        let instafeedSlider = null;

        const instafeed = new Instafeed({
            get: 'user',
            userId: userId,
            limit: 10,
            resolution: 'standard_resolution',
            accessToken: accessToken,
            sortBy: 'most-recent',
            template: `
                <div class="swiper-slide">
                    <div class="swiper-lazy cover-center" style="background-image: url({{image}})"></div>
                </div>
            `,
            success: () => {
                setTimeout(() => {
                    instafeedSlider = new Swiper('.instagram .swiper-container', {
                        slidesPerView: "auto",
                        spaceBetween: 5,
                        autoplay: {
                            delay: 10000
                        }
                    });
                }, 100);
            }
        }).run();
    }

    // Category page
    const rangeSlider = $(".range-input");

    if (rangeSlider.length > 0) {
        const min = rangeSlider.data("min");
        const max = rangeSlider.data("max");

        rangeSlider.jRange({
            from: parseFloat(min),
            to: parseFloat(max),
            step: 1,
            isRange : true
        });
    };

    // ***************************
    const experiencesGrid = $(".experiences-grid");
    
    if (experiencesGrid.length > 0) {
        const changeView = $(".change-view a");

        changeView.click(function() {
            const val = $(this).data("view");

            changeView.removeClass("active");
            $(this).addClass("active");
            experiencesGrid.removeClass("card list").addClass(val);
            localStorage.setItem("view-mode", val);
        });

        const viewMode = localStorage.getItem("view-mode");

        if (viewMode === null) {
            console.log("View mode: card.");
        } else {
            experiencesGrid.addClass(viewMode);
            changeView.removeClass("active");
            $(`a[data-view=${viewMode}]`).addClass("active");
        }
    }

    // ***************************

    const filtersWrapper = $(".filters-wrapper");
    const toggleFilters = $(".toggle-filters");

    toggleFilters.click(function() {
        filtersWrapper.toggleClass("active");
        contentOverlay.fadeToggle(200);
    });

    // Category inner page
    const galleryThumbs = new Swiper('.gallery-thumbs', {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        spaceBetween: 10,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            480: {
                slidesPerView: 5
            },
            300: {
                slidesPerView: 4
            }
        }
    });

    const galleryTop = new Swiper('.gallery-top', {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs
        }
    });

    if ($("#map").length > 0) {
        $(".rater").starrr({
            rating: 4,
            change: function(e, value){
                $(".rater .val").text(value);
            }
        })

        ymaps.ready(initMap);
    }

    // Account sidebar toggler
    const accoutSiderbarWrapper = $(".account-sidebar-wrapper");
    const toggleAccountSidebar = $(".toggle-account-sidebar");

    toggleAccountSidebar.click(() => {
        accoutSiderbarWrapper.toggleClass("active");
        contentOverlay.fadeToggle(200);
    });

    // Content overlay click handler
    const contentOverlay = $(".content-overlay");

    contentOverlay.click(function() {
        $(this).fadeOut(200);
        filtersWrapper.removeClass("active");
        accoutSiderbarWrapper.removeClass("active");
    });

    // Checkout page
    const checkoutPage = $(".cart-page.checkout");
    
    if (checkoutPage.length > 0) {
        const paymentOptionField = $("input[name='payment-option']");
        const bankCardData = $(".card-data");

        paymentOptionField.change((e) => {
            let val = e.target.value;

            if (val === "0") {
                bankCardData.hide();
            } else {
                bankCardData.show();
            }
        });
    };

    // Tooltips
    $('[data-toggle="tooltip"]').tooltip()

    // Alerts
    $(".close-alert").on("click", function() {
        $(this).closest(".site-alert").hide();
    });

});
